#pragma once

namespace Salvia {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::ListBox^  LstBoxDevices;
	protected: 
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  LblTerm;

	protected: 

	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->LstBoxDevices = (gcnew System::Windows::Forms::ListBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->LblTerm = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			this->LstBoxDevices->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->LstBoxDevices->FormattingEnabled = true;
			this->LstBoxDevices->ItemHeight = 16;
			this->LstBoxDevices->Items->AddRange(gcnew cli::array< System::Object^  >(3) {L"cpu 1", L"cpu 2", L"gpu 1"});
			this->LstBoxDevices->Location = System::Drawing::Point(-1, 29);
			this->LstBoxDevices->Name = L"LstBoxDevices";
			this->LstBoxDevices->Size = System::Drawing::Size(295, 100);
			this->LstBoxDevices->TabIndex = 0;
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->label1->ForeColor = System::Drawing::Color::RoyalBlue;
			this->label1->Location = System::Drawing::Point(12, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(162, 16);
			this->label1->TabIndex = 1;
			this->label1->Text = L"�������� ���������";
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->label2->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(192)), static_cast<System::Int32>(static_cast<System::Byte>(0)), 
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->label2->Location = System::Drawing::Point(12, 151);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(202, 16);
			this->label2->TabIndex = 2;
			this->label2->Text = L"����������� ����������:";
			this->LblTerm->AutoSize = true;
			this->LblTerm->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Underline)), 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			this->LblTerm->ForeColor = System::Drawing::Color::Red;
			this->LblTerm->Location = System::Drawing::Point(220, 151);
			this->LblTerm->Name = L"LblTerm";
			this->LblTerm->Size = System::Drawing::Size(47, 16);
			this->LblTerm->TabIndex = 3;
			this->LblTerm->Text = L"-274�";
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(292, 186);
			this->Controls->Add(this->LblTerm);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->LstBoxDevices);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
			this->Name = L"Form1";
			this->Text = L"Salvia";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	};
}

